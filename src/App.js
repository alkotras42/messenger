import React, { useEffect } from 'react'
import { BrowserRouter as Router, Switch, Route } from 'react-router-dom'
import { connect, useDispatch } from 'react-redux'

import { SignUp, Dashboard, Login } from './Pages'
import PrivateRoute from './Components/PrivateRoute'
import { authObserver } from './Redux/actions'
import Loader from './Components/Loader/'

function App({ isLoading }) {
  const dispatch = useDispatch()

  useEffect(() => {
    const unsubscribe = dispatch(authObserver())
    return unsubscribe
  })

  return (
    <div style={{ minHeight: '100vh', paddingTop: '10px' }}>
      {isLoading ? (
        <Loader />
      ) : (
        <Router>
          <Switch>
            <Route path="/singup" component={SignUp} />
            <Route path="/login" component={Login} />
            <PrivateRoute exact path="/" component={Dashboard} />
          </Switch>
        </Router>
      )}
    </div>
  )
}

const mapStateToProps = (state) => {
  return { isLoading: state.auth.isLoading }
}

export default connect(mapStateToProps, null)(App)
