import React from 'react'
import { connect } from 'react-redux'
import { Redirect, Route } from 'react-router-dom'

function PrivateRoute({ component: Component, authUser, ...rest }) {
  return (
    <Route
      {...rest}
      render={(props) => {
        return authUser ? <Component {...props} /> : <Redirect to="/login" />
      }}
    ></Route>
  )
}

const mapStateToProps = (state) => {
  return {
    authUser: state.auth.authUser,
  }
}

export default connect(mapStateToProps)(PrivateRoute)
