import React, { useEffect, useState } from 'react'
import { Container, Row, Card, Col, InputGroup, Form, Button } from 'react-bootstrap'
import { useList } from 'react-firebase-hooks/database'
import { FontAwesomeIcon } from '@fortawesome/react-fontawesome'
import { faSearch } from '@fortawesome/free-solid-svg-icons'
import Rating from '@material-ui/lab/Rating'

import firebase from '../../Firebase/'
import './ChatList.css'

export const ChatList = ({ onClick }) => {
  const messagesRef = firebase.database().ref('messenger')
  const [snapshot, loading] = useList(messagesRef)

  const snapshotValues = snapshot.map((v) => v.val())
  const [newSnapshot, setNewSnapshot] = useState(snapshotValues)
  const [statusFilter, setStatusFilter] = useState('')

  useEffect(() => setNewSnapshot(snapshotValues), [loading, snapshot])

  // Фильтр поиска в имени пользователя
  const findInName = (item, value) =>
    item.clientName.toLowerCase().indexOf(value.toLowerCase().trim()) !== -1

  // Фильтр поиска в сообщениях
  const findInMessages = (item, value) => {
    // Фильтруем сообщения в объекте, если совпадений нет, то получаем пустой массив
    const filteredMessages = item.messages.filter(
      (item) => item.content.toLowerCase().indexOf(value.toLowerCase()) !== -1
    )

    // Если массив пустой, возвращает false, иначе true
    return !!filteredMessages.length
  }

  const handleChange = (event) => {
    setNewSnapshot(
      snapshotValues.filter(
        (item) =>
          findInName(item, event.target.value) || findInMessages(item, event.target.value)
      )
    )
  }

  const handleStatusButton = (event) => {
    setStatusFilter(event.target.id)
  }

  const handleEnter = (id) => {
    messagesRef.child(id).update({
      status: 'active',
      operatorId: 1,
    })
  }

  return (
    <Container className="chat-list">
      <InputGroup>
        <InputGroup.Text>
          <FontAwesomeIcon icon={faSearch} />
        </InputGroup.Text>
        <Form.Control type="search" name="search" onChange={handleChange} />
      </InputGroup>
      <Row>
        <Col>
          <Button onClick={handleStatusButton} id="">
            Все
          </Button>
        </Col>
        <Col>
          <Button onClick={handleStatusButton} id="active">
            Активные
          </Button>
        </Col>
        <Col>
          <Button onClick={handleStatusButton} id="idle">
            В очереди
          </Button>
        </Col>
        <Col>
          <Button onClick={handleStatusButton} id="complete">
            Завершенные
          </Button>
        </Col>
      </Row>
      {!loading &&
        newSnapshot &&
        newSnapshot
          .filter((item) => item.status.indexOf(statusFilter) !== -1)
          .map((item) => (
            <Row key={item.dialogId}>
              <Col>
                <Card onClick={() => onClick(item)}>
                  <p>{item.clientName}</p>
                  {item.status === 'idle' ? (
                    <Button
                      className="bnt-enter"
                      onClick={() => handleEnter(item.dialogId)}
                    >
                      Войти
                    </Button>
                  ) : null}
                </Card>
                {item.status === 'complete' ? (
                  <div className="rating">
                    <Rating readOnly name="read-only" value={item.rating} />
                  </div>
                ) : null}
              </Col>
            </Row>
          ))}
    </Container>
  )
}
