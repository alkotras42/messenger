import React, { useState } from 'react'
import { Card, InputGroup, ListGroup, Form } from 'react-bootstrap'
import { useList } from 'react-firebase-hooks/database'
import { FontAwesomeIcon } from '@fortawesome/react-fontawesome'
import { faChevronRight } from '@fortawesome/free-solid-svg-icons'
import moment from 'moment'
import 'moment/locale/ru'
import InfiniteScroll from 'react-infinite-scroll-component'
import { useWindowHeight } from '@react-hook/window-size'

import firebase from '../../Firebase/'
import './Chat.css'
import Loader from '../Loader/'

export const Chat = ({ id }) => {
  const messagesRef = firebase.database().ref(`messenger/${id}`)
  const [snapshot, loading] = useList(messagesRef)
  const [value, setValue] = useState()
  const height = useWindowHeight({ wait: 10 })
  moment.locale('ru')

  let dialog = {}

  snapshot.map((v) => {
    dialog = { ...dialog, [v.key]: v.val() }
  })

  const handleChange = (e) => {
    setValue(e.target.value)
  }

  const handleSubmit = (e) => {
    e.preventDefault()
    messagesRef.child('messages').push({
      content: value,
      timestamp: Date.now(),
      writtenBy: 'operator',
    })
  }

  return (
    <div className="chat">
      {
        // Если диалоги еще не загрузились
        loading ? (
          <Loader />
        ) : // Если диалоги загрузились, но ни один из них не выбран
        id === null ? (
          <h4>Выбирите диалог</h4>
        ) : (
          <div>
            <Card className="chat__messages-wrapper">
              <InfiniteScroll
                dataLength={8}
                scrollableTarget=".chat__messages-wrapper"
                height={height - 110}
              >
                <ListGroup variant="flush">
                  {dialog.messages &&
                    Object.values(dialog.messages).map((v) => (
                      <ListGroup.Item
                        className={v.writtenBy + ' chat-item'}
                        key={v.timestamp}
                      >
                        <div className="chat-item__content">
                          <p>{v.content}</p>
                          <div className="chat-item__time">
                            <p>{moment(v.timestamp).fromNow()}</p>
                          </div>
                        </div>
                      </ListGroup.Item>
                    ))}
                </ListGroup>
              </InfiniteScroll>
            </Card>
            {dialog.status === 'complete' ? (
              <div className="dialog-complete">
                <span>Диалог завершен</span>
              </div>
            ) : (
              <div className="chat__input-wrapper">
                <InputGroup>
                  <Form.Control type="chat" name="chat" onChange={handleChange} />
                  <InputGroup.Text>
                    <FontAwesomeIcon icon={faChevronRight} onClick={handleSubmit} />
                  </InputGroup.Text>
                </InputGroup>
              </div>
            )}
          </div>
        )
      }
    </div>
  )
}
