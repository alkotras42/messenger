import { all } from '@redux-saga/core/effects'

import { sagaAuthWatcher } from './sagas'

export default function* rootSaga() {
  yield all([sagaAuthWatcher()])
}
