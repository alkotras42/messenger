import { takeLatest, put, call } from 'redux-saga/effects'

import * as types from '../Redux/types'
import { login, logout, registration } from '../Firebase/FirebaseAuth'

export function* sagaAuthWatcher() {
  yield takeLatest(types.FETCH_REGISTRATION, registerSaga)
  yield takeLatest(types.FETCH_LOGIN, loginSaga)
  yield takeLatest(types.FETCH_LOGOUT, logoutSaga)
}

function* registerSaga({ payload }) {
  try {
    yield call(registration, payload.email, payload.password)
  } catch (e) {
    yield put({ type: types.FETCH_REGISTRATION_FAILURE, payload: e.message })
  }
}

function* loginSaga({ payload }) {
  try {
    yield call(login, payload.email, payload.password)
  } catch (e) {
    yield put({ type: types.FETCH_LOGIN_FAILURE, payload: e.message })
  }
}

function* logoutSaga() {
  try {
    yield call(logout)
  } catch (e) {
    yield put({ type: types.FETCH_LOGOUT_FAILURE, payload: e.message })
  }
}
