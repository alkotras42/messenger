import React from 'react'
import { Formik, ErrorMessage } from 'formik'
import { Container, Form, Card, Button, Alert, InputGroup } from 'react-bootstrap'
import { Link, Redirect } from 'react-router-dom'
import { useDispatch, useSelector } from 'react-redux'
import { FontAwesomeIcon } from '@fortawesome/react-fontawesome'
import { faEnvelope, faUnlockAlt } from '@fortawesome/free-solid-svg-icons'

import { ValidationSchema } from '../Services/ValidationSchema'
import { loginUser } from '../Redux/actions'

function Login() {
  const { authUser, authError, isLoading } = useSelector((state) => ({
    authUser: state.auth.authUser,
    authError: state.auth.authError,
    isLoading: state.auth.isLoading,
  }))

  const initValues = { email: '', password: '' }

  const dispatch = useDispatch()

  const handleSubmit = (values) => {
    dispatch(loginUser(values))
  }

  if (authUser) {
    return <Redirect to="/" />
  }

  return (
    <Container
      className="d-flex align-items-center justify-content-center"
      style={{ minHeight: '100vh' }}
    >
      <Card className="w-100" style={{ maxWidth: '400px' }}>
        <Card.Body>
          <h2 className="text-center mb-4">Login Page</h2>
          {authError ? <Alert variant="danger">{authError}</Alert> : null}
          <Formik
            initialValues={initValues}
            validationSchema={ValidationSchema}
            onSubmit={handleSubmit}
          >
            {({ values, handleBlur, handleChange, handleSubmit }) => (
              <Form onSubmit={handleSubmit}>
                <Form.Group id="email">
                  <Form.Label>Email</Form.Label>
                  <InputGroup>
                    <InputGroup.Text>
                      <FontAwesomeIcon icon={faEnvelope} />
                    </InputGroup.Text>
                    <Form.Control
                      type="email"
                      name="email"
                      onChange={handleChange}
                      onBlur={handleBlur}
                      value={values.email}
                      required
                    />
                  </InputGroup>
                  <ErrorMessage name="email" component={Form.Text} />
                </Form.Group>
                <Form.Group id="password">
                  <Form.Label>Пароль</Form.Label>
                  <InputGroup>
                    <InputGroup.Text>
                      <FontAwesomeIcon icon={faUnlockAlt} />
                    </InputGroup.Text>
                    <Form.Control
                      type="password"
                      name="password"
                      onChange={handleChange}
                      onBlur={handleBlur}
                      value={values.password}
                      required
                    />
                  </InputGroup>
                  <ErrorMessage name="password" component={Form.Text} />
                </Form.Group>
                <Button
                  disabled={isLoading}
                  variant="primary"
                  className="w-100"
                  type="submit"
                  style={{ marginTop: '20px' }}
                >
                  Log In
                </Button>
              </Form>
            )}
          </Formik>
          <div className="w-100 text-center mt-3">
            Нет аккаунта? <Link to="/singup">Зарегестрироваться</Link>
          </div>
        </Card.Body>
      </Card>
    </Container>
  )
}

export default Login
