import React, { useState } from 'react'
import { Button } from 'react-bootstrap'
import { useDispatch, useSelector } from 'react-redux'

import Chat from '../Components/Chat/'
import ChatList from '../Components/ChatList/'
import Profile from '../Components/Profile/'
import { logout } from '../Redux/actions'

function Dashboard() {
  const [dialog, setDialog] = useState(null)

  const { authUser } = useSelector((state) => ({
    authUser: state.auth.authUser,
  }))

  const onClick = (item) => {
    setDialog(item.dialogId)
  }

  const dispatch = useDispatch()

  return (
    <React.Fragment>
      <header>
        <div style={{ margin: '0px 33%', boxShadow: '0px 4px 4px #f5f8ff' }}>
          <strong>Email: </strong>
          {authUser && authUser.email}
          <Button
            style={{ marginLeft: '150px' }}
            variant="link"
            onClick={() => dispatch(logout())}
          >
            Logout
          </Button>
        </div>
      </header>
      <div className="d-flex w-100">
        <ChatList onClick={onClick} />
        <Chat id={dialog} />
        <Profile />
      </div>
    </React.Fragment>
  )
}

export default Dashboard
