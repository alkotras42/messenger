import firebase from '../Firebase/'
import * as types from './types'

export const authObserver = () => (dispatch) => {
  return firebase.auth().onAuthStateChanged((user) => {
    if (user) {
      dispatch({
        type: types.FETCH_LOGIN_SUCCESS,
        payload: user,
      })
    } else {
      dispatch({
        type: types.FETCH_LOGOUT_SUCCESS,
      })
    }
  })
}

export const registerUser = (newUser) => {
  return { type: types.FETCH_REGISTRATION, payload: newUser }
}

export const loginUser = (user) => {
  return { type: types.FETCH_LOGIN, payload: user }
}

export const logout = () => {
  return { type: types.FETCH_LOGOUT }
}
