import firebase from '../Firebase/'
import * as types from './types'

const initialState = {
  isLoading: true,
  authUser: firebase.auth().currentUser,
  authError: '',
}

const authReducer = (state = initialState, action) => {
  switch (action.type) {
    case types.FETCH_REGISTRATION:
      return { ...state, isLoading: true }
    case types.FETCH_REGISTRATION_SUCCESS:
      return {
        ...state,
        isLoading: false,
        authUser: action.payload.user,
      }
    case types.FETCH_REGISTRATION_FAILURE:
      return {
        ...state,
        isLoading: false,
        authUser: null,
        authError: action.payload,
      }
    case types.FETCH_LOGIN:
      return { ...state, isLoading: true }
    case types.FETCH_LOGIN_SUCCESS:
      return { ...state, isLoading: false, authUser: action.payload }
    case types.FETCH_LOGIN_FAILURE:
      return {
        ...state,
        isLoading: false,
        authUser: null,
        authError: action.payload,
      }
    case types.FETCH_LOGOUT:
      return { ...state, isLoading: true }
    case types.FETCH_LOGOUT_SUCCESS:
      return { ...state, isLoading: false, authUser: null }
    case types.FETCH_LOGOUT_FAILURE:
      return {
        ...state,
        isLoading: false,
        authError: 'Неудалось выйти из аккаунта',
      }
    default:
      return state
  }
}

export default authReducer
