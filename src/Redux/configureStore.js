import { applyMiddleware, compose, createStore } from 'redux'
import createSagaMiddleware from 'redux-saga'
import thunk from 'redux-thunk'

import rootReducer from './rootReducer'
import rootSaga from '../Sagas'

const sagaMiddleware = createSagaMiddleware()

const configureStore = createStore(
  rootReducer,
  compose(
    applyMiddleware(thunk),
    applyMiddleware(sagaMiddleware),
    window.__REDUX_DEVTOOLS_EXTENSION__ && window.__REDUX_DEVTOOLS_EXTENSION__()
  )
)

sagaMiddleware.run(rootSaga)

export default configureStore
