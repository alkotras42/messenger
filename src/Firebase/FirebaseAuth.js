import firebase from './'

export const login = (email, password) => {
  try {
    const responce = firebase.auth().signInWithEmailAndPassword(email, password)
    return responce
  } catch (e) {
    return e
  }
}

export const registration = (email, password) => {
  return firebase.auth().createUserWithEmailAndPassword(email, password)
}

export const logout = () => {
  return firebase.auth().signOut()
}
